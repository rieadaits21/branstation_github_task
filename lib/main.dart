import 'package:brainstation_github_task/di/injection.dart' as di;
import 'package:brainstation_github_task/repo_details/presentation/bloc/repo_details_bloc.dart';
import 'package:brainstation_github_task/repo_list/presentation/bloc/search_repo/search_repo_bloc.dart';
import 'package:brainstation_github_task/repo_list/presentation/pages/homepage/homepage.dart';
import 'package:brainstation_github_task/user_repo_list/presentation/bloc/user_repo_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import 'common/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => di.locator<SearchRepoBloc>(),
        ),
        BlocProvider(
          create: (_) => di.locator<UserRepoBloc>(),
        ),
        BlocProvider(
          create: (_) => di.locator<RepoDetailsBloc>(),
        ),
      ],
      child: MaterialApp(
        title: 'Github Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData.light().copyWith(
          primaryColor: kWhiteColor,
          textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme),
          colorScheme: kColorScheme.copyWith(secondary: kWhiteColor),
          bottomNavigationBarTheme: bottomNavigationBarTheme,
        ),
        home: const MyHomePage(title: "Github User's For Flutter"),
      ),
    );
  }
}