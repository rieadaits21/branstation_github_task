import 'package:brainstation_github_task/repo_details/domain/usecases/get_repo_details.dart';
import 'package:brainstation_github_task/repo_details/presentation/bloc/repo_details_bloc.dart';
import 'package:brainstation_github_task/repo_list/data/remote_data_source/repo_remote_data_source.dart';
import 'package:brainstation_github_task/repo_list/data/repositories/repo_repository_impl.dart';
import 'package:brainstation_github_task/repo_list/domain/repositories/repos_repository.dart';
import 'package:brainstation_github_task/repo_list/domain/usecases/get_searched_repos.dart';
import 'package:brainstation_github_task/repo_list/presentation/bloc/search_repo/search_repo_bloc.dart';
import 'package:brainstation_github_task/user_repo_list/domain/repository/user_repos_repository.dart';
import 'package:brainstation_github_task/user_repo_list/domain/usecases/get_user_repos.dart';
import 'package:brainstation_github_task/user_repo_list/presentation/bloc/user_repo_bloc.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';

import 'package:http/http.dart' as http;

import '../common/network_info.dart';
import '../repo_details/data/remote_data_source/repos_details_data_sourse.dart';
import '../repo_details/data/repositories/repo_details_repository_impl.dart';
import '../repo_details/domain/repository/repo_details_repository.dart';
import '../user_repo_list/data/remote_data_source/user_repos_remote_data_sourse.dart';
import '../user_repo_list/data/repository/user_repos_repository_impl.dart';

final locator = GetIt.instance;

void init() {
  final http.Client httpClient = http.Client();

  //bloc
  locator.registerFactory(() => SearchRepoBloc(locator()));
  locator.registerFactory(() => UserRepoBloc(locator()));
  locator.registerFactory(() => RepoDetailsBloc(locator()));

  //usecase
  locator.registerLazySingleton(() => GetSearchedRepos(locator()));
  locator.registerLazySingleton(() => GetUserRepos(locator()));
  locator.registerLazySingleton(() => GetRepoDetails(locator()));

  //repository
  locator.registerLazySingleton<ReposRepository>(
    () => RepoRepositoryImpl(
      remoteDataSource: locator(),
      networkInfo: locator(),
    ),
  );
  locator.registerLazySingleton<UserReposRepository>(
    () => UserRepoRepositoryImpl(
      remoteDataSource: locator(),
      networkInfo: locator(),
    ),
  );
  locator.registerLazySingleton<RepoDetailsRepository>(
    () => RepoDetailsRepositoryImpl(
      remoteDataSource: locator(),
      networkInfo: locator(),
    ),
  );

  //dara source
  locator.registerLazySingleton<RepoRemoteDataSource>(
      () => RepoRemoteDataSourceImpl(client: locator()));
  locator.registerLazySingleton<UserReposRemoteDataSource>(
      () => UserRepoRemoteDataSourceImpl(client: locator()));
  locator.registerLazySingleton<ReposDetailsRemoteDataSource>(
      () => ReposDetailsRemoteDataSourceImpl(client: locator()));

  // network info
  locator.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(locator()));

  locator.registerLazySingleton<http.Client>(() => httpClient);
  locator.registerLazySingleton(() => DataConnectionChecker());
}
