import 'package:brainstation_github_task/repo_list/presentation/bloc/search_repo/search_repo_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import '../../../../common/config.dart';
import '../../../../common/theme.dart';
import '../../../../common/utils.dart';
import '../../../../user_repo_list/presentation/page/user_repos_page.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int currentPage = 1;
  int totalPage = 0;

  @override
  void initState() {
    super.initState();
    Future.microtask(() => context
        .read<SearchRepoBloc>()
        .add(const OnQueryChanged("stars", false)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text(
            widget.title,
            style: const TextStyle(color: Colors.white),
          ),
        ),
        body: BlocConsumer<SearchRepoBloc, SearchRepoState>(
          listener: (context, state) {
            if (state is SearchRepoError) {
              showToast(state.message);
            }
          },
          builder: (context, state) {
            if (state is SearchRepoInitial) {
              return const Center(
                child: Text("hello"),
              );
            } else if (state is SearchRepoLoading) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is SearchRepoHasData) {
              currentPage = state.currentPage;
              final result = state.searchResult;
              totalPage = (state.totalResult / pageSize).ceil();
              return LazyLoadScrollView(
                onEndOfPage: () {
                  if (currentPage < totalPage) {
                    context.read<SearchRepoBloc>().add(OnNextPage(
                        state.query, currentPage, state.switchValue));
                  } else {
                    showToast("No more data found");
                  }
                },
                scrollOffset: 200,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text("Sort Repo By Updated or Star Count"),
                          Switch(
                              inactiveThumbColor: kPrimaryColor,
                              activeColor: kBlackColor,
                              value: state.switchValue,
                              onChanged: (value) {
                                if (value) {
                                  context
                                      .read<SearchRepoBloc>()
                                      .add(OnToggleChanged("updated", value));
                                } else {
                                  context
                                      .read<SearchRepoBloc>()
                                      .add(OnToggleChanged("stars", value));
                                }
                              }),
                        ],
                      ),
                      Expanded(
                        child: ListView.builder(
                          key: const Key('search_item'),
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          itemBuilder: (context, index) {
                            final repo = result[index];
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        UserReposPage(userName: repo.owner.name!),
                                  ),
                                );
                              },
                              child: Container(
                                margin: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    color: kInactiveColor,
                                    borderRadius: BorderRadius.circular(20)),
                                child: SizedBox(
                                    height: 100,
                                    width: double.maxFinite,
                                    child: Center(
                                        child: Text("${repo.owner.name}"))),
                              ),
                            );
                          },
                          itemCount: result.length,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            } else {
              return Center(
                child: Text(state.runtimeType.toString()),
              );
            }
          },
        ));
  }
}
