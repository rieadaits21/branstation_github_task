import 'package:flutter/material.dart';

import '../../../../common/theme.dart';

class MyErrorWidget extends StatelessWidget {
  final String message;

  const MyErrorWidget({Key? key,  required this.message }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildError(context, message);
  }

  Widget _buildError(BuildContext context, String message) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [

        const SizedBox(height:8),
        Text(
          message, 
          style: primaryTextStyle,
        )
      ],
    );
  }
}