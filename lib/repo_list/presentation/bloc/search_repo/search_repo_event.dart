part of 'search_repo_bloc.dart';


abstract class SearchRepoEvent extends Equatable {
  const SearchRepoEvent();

  @override
  List<Object> get props => [];
}

class OnToggleChanged extends SearchRepoEvent {
  final String query;
  final bool switchValue;

  const OnToggleChanged(this.query, this.switchValue);
 
  @override
  List<Object> get props => [query, switchValue];
}

class OnQueryChanged extends SearchRepoEvent {
  final String query;
  final bool switchValue;

  const OnQueryChanged(this.query, this.switchValue);

  @override
  List<Object> get props => [query, switchValue];
}

class OnNextPage extends SearchRepoEvent {
  final String query;
  final int page;
  final bool switchValue;

  const OnNextPage(
    this.query,
    this.page,
    this.switchValue,
  );

  @override
  List<Object> get props => [query, page,switchValue];
}
