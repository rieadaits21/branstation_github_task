part of 'search_repo_bloc.dart';

abstract class SearchRepoState extends Equatable {
  const SearchRepoState();

  @override
  List<Object> get props => [];
}

class SearchRepoInitial extends SearchRepoState {}

class SearchRepoEmpty extends SearchRepoState {
  final String message;

  const SearchRepoEmpty(this.message);

  @override
  List<Object> get props => [message];
}

class SearchRepoLoading extends SearchRepoState {}

class SearchRepoHasData extends SearchRepoState {
  final List<Repo> searchResult;
  final int totalResult;
  final int currentPage;
  final bool switchValue;
  final String query;

  const SearchRepoHasData({
    required this.searchResult,
    required this.totalResult,
    required this.query,
    required this.currentPage,
    this.switchValue = false,
  });

  @override
  List<Object> get props =>
      [searchResult, totalResult, currentPage, query, switchValue];
}

class SearchRepoError extends SearchRepoState {
  final String message;

  const SearchRepoError(this.message);

  @override
  List<Object> get props => [message];
}
