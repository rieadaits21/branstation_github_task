import 'package:brainstation_github_task/repo_list/domain/usecases/get_searched_repos.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:brainstation_github_task/repo_list/domain/entity/repo.dart';
import 'package:equatable/equatable.dart';

part 'search_repo_event.dart';

part 'search_repo_state.dart';

class SearchRepoBloc extends Bloc<SearchRepoEvent, SearchRepoState> {
  final GetSearchedRepos _searchRepos;
  List<Repo> repos = [];

  SearchRepoBloc(this._searchRepos) : super(SearchRepoInitial()) {
    on<OnQueryChanged>(
      (event, emit) async {
        repos = [];
        final query = event.query;
        if (query.isEmpty) {
          emit(SearchRepoInitial());
        } else {
          emit(SearchRepoLoading());
          final result = await _searchRepos.execute(query);
          result.fold((failure) => emit(SearchRepoError(failure.message)),
              (reposData) {
            repos = reposData.items;
            emit(SearchRepoHasData(
              searchResult: reposData.items,
              totalResult: reposData.totalCount,
              query: query,
              currentPage: 1,
              switchValue: event.switchValue,
            ));
            if (reposData.items.isEmpty) {
              emit(const SearchRepoEmpty('No Result Found'));
            }
          });
        }
      },
    );

    on<OnToggleChanged>(
      (event, emit) async {
        repos = [];
        final query = event.query;
        if (query.isEmpty) {
          emit(SearchRepoInitial());
        } else {
          emit(SearchRepoLoading());
          final result = await _searchRepos.execute(query);
          result.fold((failure) => emit(SearchRepoError(failure.message)),
              (reposData) {
            repos = reposData.items;
            emit(SearchRepoHasData(
              searchResult: reposData.items,
              totalResult: reposData.totalCount,
              query: query,
              currentPage: 1,
              switchValue: event.switchValue,
            ));
            if (reposData.items.isEmpty) {
              emit(const SearchRepoEmpty('No Result Found'));
            }
          });
        }
      },
    );

    on<OnNextPage>(
      (event, emit) async {
        final query = event.query;
        final page = event.page + 1;
        if (query.isEmpty) {
          emit(SearchRepoInitial());
        } else {
          final result = await _searchRepos.execute(query, page: page);
          result.fold((failure) => emit(SearchRepoError(failure.message)),
              (reposData) {
            repos.addAll(reposData.items);
            emit(SearchRepoHasData(
              searchResult: repos,
              totalResult: reposData.totalCount,
              query: query,
              currentPage: page,
              switchValue: event.switchValue,

            ));
            if (reposData.items.isEmpty) {
              emit(const SearchRepoEmpty('No Result Found'));
            }
          });
        }
      },
    );
  }
}
