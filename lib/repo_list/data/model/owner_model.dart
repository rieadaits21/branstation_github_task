import 'package:brainstation_github_task/repo_list/domain/entity/owner.dart';
import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class OwnerModel extends Equatable{
	String? name;
	String? avatarUrl;

	OwnerModel({this.name, this.avatarUrl,});

	OwnerModel.fromJson(Map<String, dynamic> json) {
		name = json['login'];
		avatarUrl = json['avatar_url'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = <String, dynamic>{};
		data['login'] = name;
		data['avatar_url'] = avatarUrl;
		return data;
	}

	Owner toEntity() {
		return Owner(
			name: name,
			avatarUrl: avatarUrl,
		);
	}

  @override
  List<Object?> get props => [name, avatarUrl,];
}