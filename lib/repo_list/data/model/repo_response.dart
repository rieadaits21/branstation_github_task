import 'package:equatable/equatable.dart';

import '../../domain/entity/repos.dart';
import 'repo_model.dart';

class RepoResponse extends Equatable {
  final int totalCount;
  final List<RepoModel> items;

  const RepoResponse({
    required this.totalCount,
    required this.items,
  });

  factory RepoResponse.fromJson(Map<String, dynamic> json) => RepoResponse(
    totalCount: json['total_count'],
    items: List<RepoModel>.from((json["items"] as List)
      .map((x) => RepoModel.fromJson(x)).toList()
      .where((article) => 
        article.repoName != null &&
        article.description != null,),),
  );

  Map<String, dynamic> toJson() => {
    "total_count": totalCount,
    "items": List<dynamic>.from(items.map((x) => x.toJson()))
  };

  Repos toEntity() {
    return Repos(
      totalCount: totalCount,
      items: items.map((article) => article.toEntity()).toList(),
    );
  } 

  @override
  List<Object?> get props => [
    totalCount,
    items
  ];
}