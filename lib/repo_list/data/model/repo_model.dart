import 'package:brainstation_github_task/repo_list/data/model/owner_model.dart';
import 'package:equatable/equatable.dart';

import '../../domain/entity/repo.dart';

class RepoModel extends Equatable {
  const RepoModel({
    required this.repoName,
    required this.description,
    required this.updatedAt,
    required this.totalStarred,
    required this.openIssueCount,
    required this.forks,
    required this.ownerModel,
  });


 final String? repoName;
  final String? description;
  final DateTime updatedAt;
  final int totalStarred;
  final int openIssueCount;
  final int forks;
  final OwnerModel ownerModel;

  factory RepoModel.fromJson(Map<String, dynamic> json) => RepoModel(
        repoName: json["name"],
        description: json["description"] ?? "-",
    openIssueCount: json["open_issues_count"],
        totalStarred: json["stargazers_count"],
    updatedAt: DateTime.parse(json["updated_at"]),
    forks: json["forks_count"],
    ownerModel: OwnerModel.fromJson(json["owner"]),
      );

  Map<String, dynamic> toJson() => {
        "name": repoName,
        "owner": ownerModel.toJson(),
        "description": description,
        "forks_count": forks,
        "stargazers_count": totalStarred,
        "updated_at": updatedAt.toIso8601String(),
        "open_issues_count": openIssueCount,
      };

  Repo toEntity() {
    return Repo(
      repoName: repoName,
      owner: ownerModel.toEntity(),
      description: description,
      updatedAt: updatedAt,
      totalStarred: totalStarred,
      forks: forks,
      openIssueCount: openIssueCount,
    );
  } 

  @override
  List<Object?> get props => [
        repoName,
        ownerModel,
        description,
        updatedAt,
        totalStarred,
        forks,
        openIssueCount,
      ];
}
