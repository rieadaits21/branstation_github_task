
import 'dart:io';

import 'package:brainstation_github_task/common/failure.dart';
import 'package:brainstation_github_task/repo_list/domain/repositories/repos_repository.dart';
import 'package:dartz/dartz.dart';

import '../../../common/exception.dart';
import '../../../common/network_info.dart';
import '../../domain/entity/repos.dart';
import '../remote_data_source/repo_remote_data_source.dart';

class RepoRepositoryImpl implements ReposRepository{
  final RepoRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  RepoRepositoryImpl({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, Repos>> searchRepos(String query, {int page = 1}) async {
    try {
      final result = await remoteDataSource.searchRepos(query, page);
      return Right(result.toEntity());
    } on ServerException {
      return const Left(ServerFailure(''));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    } on TlsException catch (e) {
      return Left(CommonFailure('Certificated Not Valid:\n${e.message}'));
    }
  }

}