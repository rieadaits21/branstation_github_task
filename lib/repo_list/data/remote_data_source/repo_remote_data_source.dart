import 'dart:convert';

import 'package:brainstation_github_task/repo_list/data/model/repo_response.dart';
import 'package:http/http.dart' as http;

import '../../../common/config.dart';
import '../../../common/exception.dart';

abstract class RepoRemoteDataSource {
  Future<RepoResponse> searchRepos(String query, int page);
}

class RepoRemoteDataSourceImpl implements RepoRemoteDataSource {
  final http.Client client;

  RepoRemoteDataSourceImpl({required this.client});

  @override
  Future<RepoResponse> searchRepos(String query, int page) async {
    final response = await client.get(
      Uri.parse(
          '${baseUrl}search/repositories?q=topic:flutter&page=$page&per_page=$pageSize&sort=$query&order=desc'),
    );

    if (response.statusCode == 200) {
      return RepoResponse.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
