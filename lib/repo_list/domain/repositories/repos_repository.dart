import 'package:dartz/dartz.dart';
import 'package:brainstation_github_task/common/failure.dart';

import '../entity/repos.dart';

abstract class ReposRepository{
  Future<Either<Failure, Repos>> searchRepos(String query, {int page = 1});
}