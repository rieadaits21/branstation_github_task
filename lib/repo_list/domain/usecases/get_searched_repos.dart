
import 'package:brainstation_github_task/repo_list/domain/repositories/repos_repository.dart';
import 'package:dartz/dartz.dart';

import '../../../common/failure.dart';
import '../entity/repos.dart';

class GetSearchedRepos{
  final ReposRepository repository;
  GetSearchedRepos(this.repository);

  Future<Either<Failure, Repos>> execute(String query, {int page = 1}) {
    return repository.searchRepos(query, page: page);
  }
}