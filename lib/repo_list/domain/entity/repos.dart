import 'package:equatable/equatable.dart';

import 'repo.dart';

// ignore: must_be_immutable
class Repos extends Equatable {
  Repos({
    required this.totalCount,
    required this.items,
  });

  int totalCount;
  List<Repo> items;

  @override
  List<Object?> get props => [
    totalCount,
    items
  ];
}
