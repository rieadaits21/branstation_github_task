import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class Owner extends Equatable {
  Owner({
    required this.name,
    required this.avatarUrl,
  });

  String? name;
  String? avatarUrl;

  Owner.bookmark({
    required this.name,
    required this.avatarUrl,
  });

  @override
  List<Object?> get props => [
    name,
    avatarUrl,
  ];
}