import 'package:brainstation_github_task/repo_list/domain/entity/owner.dart';
import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class Repo extends Equatable {
  Repo({
    required this.repoName,
    required this.description,
    required this.updatedAt,
    required this.totalStarred,
    required this.openIssueCount,
    required this.forks,
    required this.owner,
  });


  String? repoName;
  String? description;
  DateTime updatedAt;
  int totalStarred;
  int openIssueCount;
  int forks;
  Owner owner;
  
  Repo.bookmark({
    required this.repoName,
    required this.updatedAt,
    required this.description,
    required this.totalStarred,
    required this.openIssueCount,
    required this.forks,
    required this.owner,
  });

  @override
  List<Object?> get props => [
    repoName,
    updatedAt,
    description,
    totalStarred,
    openIssueCount,
    forks,
    owner,
  ];
}