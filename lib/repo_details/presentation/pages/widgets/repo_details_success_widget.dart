import 'package:brainstation_github_task/repo_details/presentation/bloc/repo_details_bloc.dart';
import 'package:flutter/material.dart';

import '../../../../common/theme.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';


class RepoDetailsSuccessWidget extends StatelessWidget {
  const RepoDetailsSuccessWidget({super.key, required this.state});

  final RepoDetailsHasData state;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[400],
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.5,
              child: ShaderMask(
                shaderCallback: (rect) {
                  return LinearGradient(
                    colors: [
                      Colors.transparent,
                      Colors.black.withOpacity(0.5),
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ).createShader(
                    Rect.fromLTRB(0, 0, rect.width, rect.bottom),
                  );
                },
                blendMode: BlendMode.darken,
                child: CachedNetworkImage(
                  imageUrl: state.repoDetails.owner.avatarUrl!,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover,),
                    ),
                  ),
                  errorWidget: (context, url, error) => const SizedBox.shrink(),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 5,
                  ),
                  Text("Author name: ${state.repoDetails.owner.name}",
                    style: whiteTextStyle.copyWith(
                      fontSize: 18, fontWeight: semiBold,),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text("Repository Name: ${state.repoDetails.repoName}",
                    style: whiteTextStyle.copyWith(fontSize: 14),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text('Repo Description: ${state.repoDetails.description}',
                    style: whiteTextStyle.copyWith(fontSize: 14),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Forks: ${state.repoDetails.forks}",
                    style: whiteTextStyle.copyWith(fontSize: 14),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Starred: ${state.repoDetails.totalStarred}",
                    style: whiteTextStyle.copyWith(fontSize: 14),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                  "Last Updated: ${DateFormat('d MMMM yyyy, hh:mm:ss a').format(
                      state.repoDetails.updatedAt,)}",
                    style: whiteTextStyle.copyWith(fontSize: 14),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),

          ],
        ),
      ),
    );
  }
}
