import 'package:brainstation_github_task/repo_details/presentation/bloc/repo_details_bloc.dart';
import 'package:brainstation_github_task/repo_details/presentation/pages/widgets/repo_details_success_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common/theme.dart';
import '../../../common/utils.dart';

class RepoDetailsPage extends StatefulWidget {
  const RepoDetailsPage({
    super.key,
    required this.userName,
    required this.repoName,
  });

  final String userName;
  final String repoName;

  @override
  State<RepoDetailsPage> createState() => _RepoDetailsPageState();
}

class _RepoDetailsPageState extends State<RepoDetailsPage> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context
          .read<RepoDetailsBloc>()
          .add(OnReposDetailsGet(widget.userName, widget.repoName));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kGreyColor,
        title: Text(
          "${widget.repoName} details",
          style: const TextStyle(color: Colors.white),
        ),
      ),
      body: BlocConsumer<RepoDetailsBloc, RepoDetailsState>(
        listener: (context, state) {
          if (state is RepoDetailsError) {
            showToast(state.message);
          }
        },
        builder: (context, state) {
          if (state is RepoDetailsInitial) {
            return const Center(
              child: Text("hello"),
            );
          } else if (state is RepoDetailsLoading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is RepoDetailsHasData) {
            return RepoDetailsSuccessWidget(state: state,);
          } else {
            return Center(
              child: Text(state.runtimeType.toString()),
            );
          }
        },
      ),
    );
  }
}
