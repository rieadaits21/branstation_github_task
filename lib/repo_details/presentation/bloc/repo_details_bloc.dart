import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:brainstation_github_task/repo_list/domain/entity/repo.dart';
import 'package:equatable/equatable.dart';

import '../../domain/usecases/get_repo_details.dart';


part 'repo_details_event.dart';

part 'repo_details_state.dart';

class RepoDetailsBloc extends Bloc<RepoDetailsEvent, RepoDetailsState> {
  final GetRepoDetails _getRepoDetails;

  RepoDetailsBloc(this._getRepoDetails) : super(RepoDetailsInitial()) {

    on<OnReposDetailsGet>(
      (event, emit) async {
        final userName = event.usersName;
        final repoName = event.repoName;
        if (userName.isEmpty && repoName.isEmpty) {
          emit(RepoDetailsInitial());
        } else {
          emit(RepoDetailsLoading());
          final result = await _getRepoDetails.execute(userName, repoName);
          result.fold((failure) => emit(RepoDetailsError(failure.message)),
              (reposData) {
            emit(RepoDetailsHasData(
              repoDetails: reposData,
            ));
          });
        }
      },
    );
  }
}
