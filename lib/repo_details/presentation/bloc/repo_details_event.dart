part of 'repo_details_bloc.dart';


abstract class RepoDetailsEvent extends Equatable {
  const RepoDetailsEvent();

  @override
  List<Object> get props => [];
}

class OnReposDetailsGet extends RepoDetailsEvent {
  final String usersName;
  final String repoName;

  const OnReposDetailsGet(this.usersName,this.repoName);
 
  @override
  List<Object> get props => [usersName, repoName];
}