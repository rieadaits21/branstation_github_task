part of 'repo_details_bloc.dart';

abstract class RepoDetailsState extends Equatable {
  const RepoDetailsState();

  @override
  List<Object> get props => [];
}

class RepoDetailsInitial extends RepoDetailsState {}

class RepoDetailsLoading extends RepoDetailsState {}

class RepoDetailsHasData extends RepoDetailsState {
  final Repo repoDetails;

  const RepoDetailsHasData({
    required this.repoDetails,
  });

  @override
  List<Object> get props =>
      [repoDetails];
}

class RepoDetailsError extends RepoDetailsState {
  final String message;

  const RepoDetailsError(this.message);

  @override
  List<Object> get props => [message];
}
