
import 'dart:convert';

import '../../../repo_list/data/model/repo_model.dart';

RepoModel repoModelFromJson(String str) => RepoModel.fromJson(json.decode(str));

String repoModelToJson(RepoModel data) => json.encode(data.toJson());
