import 'dart:io';

import 'package:brainstation_github_task/common/failure.dart';
import 'package:brainstation_github_task/repo_details/data/remote_data_source/repos_details_data_sourse.dart';
import 'package:brainstation_github_task/repo_details/domain/repository/repo_details_repository.dart';
import 'package:dartz/dartz.dart';

import '../../../common/exception.dart';
import '../../../common/network_info.dart';
import '../../../repo_list/domain/entity/repo.dart';


class RepoDetailsRepositoryImpl implements RepoDetailsRepository{
  final ReposDetailsRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  RepoDetailsRepositoryImpl({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, Repo>> getRepoDetails(String userName, String repoName) async{
    try {
      final result = await remoteDataSource.getRepoDetails(userName, repoName);
      return Right(result.toEntity());
    } on ServerException {
      return const Left(ServerFailure(''));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    } on TlsException catch (e) {
      return Left(CommonFailure('Certificated Not Valid:\n${e.message}'));
    }
  }
}