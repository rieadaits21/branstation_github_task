
import 'package:http/http.dart' as http;

import '../../../common/config.dart';
import '../../../common/exception.dart';
import '../../../repo_list/data/model/repo_model.dart';
import '../model/repo_details_response.dart';

abstract class ReposDetailsRemoteDataSource {
  Future<RepoModel> getRepoDetails(String userName, String repoName);
}

class ReposDetailsRemoteDataSourceImpl implements ReposDetailsRemoteDataSource {
  final http.Client client;

  ReposDetailsRemoteDataSourceImpl({required this.client});

  @override
  Future<RepoModel> getRepoDetails(String userName, String repoName)  async {
    final response = await client.get(
      Uri.parse(
          '${baseUrl}repos/$userName/$repoName'),
    );

    if (response.statusCode == 200) {
      return repoModelFromJson(response.body);
    } else {
      throw ServerException();
    }
  }
}
