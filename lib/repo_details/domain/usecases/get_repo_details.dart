import 'package:brainstation_github_task/repo_list/domain/entity/repo.dart';
import 'package:dartz/dartz.dart';

import '../../../common/failure.dart';
import '../repository/repo_details_repository.dart';

class GetRepoDetails{
  final RepoDetailsRepository repository;
  GetRepoDetails(this.repository);

  Future<Either<Failure, Repo>> execute(String userName, String repoName) {
    return repository.getRepoDetails(userName,repoName);
  }
}