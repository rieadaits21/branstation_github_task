import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

final RouteObserver<ModalRoute> routeObserver = RouteObserver<ModalRoute>();

void showToast(String msg) =>  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.TOP,
    timeInSecForIosWeb: 1,
    backgroundColor: Colors.red,
    textColor: Colors.white,
    fontSize: 16.0
);