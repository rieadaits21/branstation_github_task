part of 'user_repo_bloc.dart';


abstract class UserRepoEvent extends Equatable {
  const UserRepoEvent();

  @override
  List<Object> get props => [];
}

class OnUserReposGet extends UserRepoEvent {
  final String usersName;

  const OnUserReposGet(this.usersName);
 
  @override
  List<Object> get props => [usersName];
}