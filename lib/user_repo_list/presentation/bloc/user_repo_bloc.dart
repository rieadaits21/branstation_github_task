import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:brainstation_github_task/repo_list/domain/entity/repo.dart';
import 'package:equatable/equatable.dart';

import '../../domain/usecases/get_user_repos.dart';

part 'user_repo_event.dart';

part 'user_repo_state.dart';

class UserRepoBloc extends Bloc<UserRepoEvent, UserRepoState> {
  final GetUserRepos _getUserRepos;

  UserRepoBloc(this._getUserRepos) : super(UserRepoInitial()) {

    on<OnUserReposGet>(
      (event, emit) async {
        final query = event.usersName;
        if (query.isEmpty) {
          emit(UserRepoInitial());
        } else {
          emit(UserRepoLoading());
          final result = await _getUserRepos.execute(query);
          result.fold((failure) => emit(UserRepoError(failure.message)),
              (reposData) {
            emit(UserRepoHasData(
              repoList: reposData,
            ));
          });
        }
      },
    );
  }
}
