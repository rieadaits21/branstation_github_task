part of 'user_repo_bloc.dart';

abstract class UserRepoState extends Equatable {
  const UserRepoState();

  @override
  List<Object> get props => [];
}

class UserRepoInitial extends UserRepoState {}

class UserRepoLoading extends UserRepoState {}

class UserRepoHasData extends UserRepoState {
  final List<Repo> repoList;

  const UserRepoHasData({
    required this.repoList,
  });

  @override
  List<Object> get props =>
      [repoList];
}

class UserRepoError extends UserRepoState {
  final String message;

  const UserRepoError(this.message);

  @override
  List<Object> get props => [message];
}
