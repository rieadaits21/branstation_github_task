import 'package:brainstation_github_task/user_repo_list/presentation/bloc/user_repo_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common/theme.dart';
import '../../../common/utils.dart';
import '../../../repo_details/presentation/pages/repo_details_page.dart';

class UserReposPage extends StatefulWidget {
  const UserReposPage({
    super.key,
    required this.userName,
  });

  final String userName;

  @override
  State<UserReposPage> createState() => _UserReposPageState();
}

class _UserReposPageState extends State<UserReposPage> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<UserRepoBloc>().add(OnUserReposGet(widget.userName));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kGreyColor,
        title: Text(
          "${widget.userName} user repo list",
          style: const TextStyle(color: Colors.white),
        ),
      ),
      body: BlocConsumer<UserRepoBloc, UserRepoState>(
        listener: (context, state) {
          if (state is UserRepoError) {
            showToast(state.message);
          }
        },
        builder: (context, state) {
          if (state is UserRepoInitial) {
            return const Center(
              child: Text("hello"),
            );
          } else if (state is UserRepoLoading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is UserRepoHasData) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.builder(
                key: const Key('search_item'),
                padding: const EdgeInsets.symmetric(vertical: 16),
                itemBuilder: (context, index) {
                  final repo = state.repoList[index];
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RepoDetailsPage(
                            repoName: repo.repoName!,
                            userName: repo.owner.name!,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: kInactiveColor,
                          borderRadius: BorderRadius.circular(20)),
                      child: SizedBox(
                          height: 100,
                          width: double.maxFinite,
                          child: Center(child: Text("${repo.repoName}"))),
                    ),
                  );
                },
                itemCount: state.repoList.length,
              ),
            );
          } else {
            return Center(
              child: Text(state.runtimeType.toString()),
            );
          }
        },
      ),
    );
  }
}
