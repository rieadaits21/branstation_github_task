
import 'package:dartz/dartz.dart';
import 'package:brainstation_github_task/common/failure.dart';

import '../../../repo_list/domain/entity/repo.dart';


abstract class UserReposRepository{
  Future<Either<Failure, List<Repo>>> getUserRepos(String query);
}