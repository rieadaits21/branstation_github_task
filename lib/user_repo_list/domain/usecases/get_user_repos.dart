import 'package:brainstation_github_task/repo_list/domain/entity/repo.dart';
import 'package:brainstation_github_task/user_repo_list/domain/repository/user_repos_repository.dart';
import 'package:dartz/dartz.dart';

import '../../../common/failure.dart';

class GetUserRepos{
  final UserReposRepository repository;
  GetUserRepos(this.repository);

  Future<Either<Failure, List<Repo>>> execute(String query) {
    return repository.getUserRepos(query);
  }
}