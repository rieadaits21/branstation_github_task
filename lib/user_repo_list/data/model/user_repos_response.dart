
import 'dart:convert';

import '../../../repo_list/data/model/repo_model.dart';

List<RepoModel> userReposResponseFromJson(String str) => List<RepoModel>.from(json.decode(str).map((x) => RepoModel.fromJson(x)));

String userReposResponseToJson(List<RepoModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

