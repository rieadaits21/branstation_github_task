
import 'dart:io';

import 'package:brainstation_github_task/common/failure.dart';
import 'package:dartz/dartz.dart';

import '../../../common/exception.dart';
import '../../../common/network_info.dart';
import '../../../repo_list/domain/entity/repo.dart';
import '../../domain/repository/user_repos_repository.dart';
import '../remote_data_source/user_repos_remote_data_sourse.dart';


class UserRepoRepositoryImpl implements UserReposRepository{
  final UserReposRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  UserRepoRepositoryImpl({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<Repo>>> getUserRepos(String query) async{
    try {
      final result = await remoteDataSource.getUserRepos(query);
      return Right(result.map((e) => e.toEntity()).toList());
    } on ServerException {
      return const Left(ServerFailure(''));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    } on TlsException catch (e) {
      return Left(CommonFailure('Certificated Not Valid:\n${e.message}'));
    }
  }

}