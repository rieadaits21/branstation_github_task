import 'package:http/http.dart' as http;

import '../../../common/config.dart';
import '../../../common/exception.dart';
import '../../../repo_list/data/model/repo_model.dart';
import '../model/user_repos_response.dart';

abstract class UserReposRemoteDataSource {
  Future<List<RepoModel>> getUserRepos(String query);
}

class UserRepoRemoteDataSourceImpl implements UserReposRemoteDataSource {
  final http.Client client;

  UserRepoRemoteDataSourceImpl({required this.client});

  @override
  Future<List<RepoModel>> getUserRepos(String query)  async {
    final response = await client.get(
      Uri.parse(
          '${baseUrl}users/$query/repos'),
    );

    if (response.statusCode == 200) {
      return userReposResponseFromJson(response.body);
    } else {
      throw ServerException();
    }
  }
}
