# brainstation_github_task

# About

A simple Flutter app that displays List of the Github Users, The users repo list and the repo details coming from Github API using Clean Architecture pattern and Bloc State Management.

#### Installation

In the command terminal, run the following commands:

    $ git clone https://gitlab.com/rieadaits21/branstation_github_task.git
    $ cd branstation_github_task
    $ flutter run

# Features 🎯

- The 'Home' screen displays a list of Github User's from Github for the Flutter topic
- By default the list of the Github User's are sorting by the "Starred" number
- There has a "Switch" button on the top corner, if the user toggle it the Github User's list will be sorted by the "Updated" time
- Clicking on any item on the user navigates the user to the "Repo list" screen for the clicked user
- Clicking on any item on the "Repo list" navigates the user to the "Repo Details" screen where user can see the "Name" and "Profile picture" of the Repo user, "Repo description", "Total Starred" for the repo, "Total Fork" number and "Last Updated" time.
- Implemented Clean Architecture as architecture pattern
- Implemented state management using Bloc pattern
- Utilized a component-wise design pattern to promote code modularity and usability
- Fetched conference data using HTTP requests from a GraphQL API using http package
- Made the app responsive for different screen sizes and orientations

## Project Config Roadmap

All the necessary config and dependencies have already been set and ready for use but there is an explanation of each step if you want to know more about the pre-config or customize it.

Initialize the Flutter project, add all the necessary dependencies mentioned above in the **pubspec.yaml** configuration file and run `pub get`.

**pubspec.yaml**
```yaml
dependencies:
  flutter:
    sdk: flutter

  cupertino_icons: ^1.0.2
  cached_network_image: ^3.0.0
  dartz: ^0.10.1
  equatable: ^2.0.5
  flutter_bloc: ^8.1.3
  flutter_spinkit: ^5.1.0
  get_it: ^7.6.7
  google_fonts: ^6.1.0
  http: ^1.1.0
  intl: ^0.19.0
  lazy_load_scrollview: ^1.3.0
  path_provider: ^2.0.7
  shimmer: ^2.0.0
  sqflite_sqlcipher: ^2.2.1
  data_connection_checker:
    git:
      url: https://github.com/chornthorn/data_connection_checker.git
      ref: master
  fluttertoast: ^8.2.4

dev_dependencies:
  flutter_test:
    sdk: flutter

  flutter_lints: ^2.0.0
```

# Unfinished Task 🎯

- Caching the loaded data from api feature is missing this version

# Future Scope 🎯

- Caching data implementation
- Search repository by User input
- Redirect github repo link from the app


#### Screenshots

<table>
    <tr>
        <td><img src='./screenshots/img1.jpg' width="300" height="600"></td>
        <td><img src='./screenshots/img4.jpg' width="300" height="600"></td>
    </tr>
    <tr>
        <td><img src='./screenshots/img2.jpg' width="300" height="600"></td>
        <td><img src='./screenshots/img3.jpg' width="300" height="600"></td>
    </tr>
</table>